# Infrastructure Declarations

Repository holding every configuration files used to configure Celduin Infrastructure.

To see how those files were generated please refer to [celduin-infra project].

# Project Structure

This project is divided into two branches (Staging, Prod) where each represent an environment.  


## Staging

The folder structure of this branch is as follow:

+ argocd  
  This folder contains all the definitions used by Argo-CD  
  - apps  
    This folder contains all the Application definitions. At its root the Applications definitions regroups several more narrowed Applications, those Application definitions are stored ina folder named the same as the top-level Application.  
  - staging.json  
    Definition for the Project.  
+ kubernetes  
  This folder contains the definitions of all the other kubernetes elements. The root of this folder is only composed of folders named after Argo-CD Applications.


# Argo CD Configuration

Argo-CD is configured to deploy elements from each branch only to the corresponding environment.  

Currently Argo-CD is not configured to monitor and deploy itself.

## Manual Commands

### Deploy Argo-CD

Deploy Argo-CD and all the CRDs with:

```
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v1.4.2/manifests/install.yaml
```

### Use argocd CLI

To connect argocd CLI to your Argo-CD use:

```
argocd login <server>:<port>
```

When running locally make sure to`port-forward` first:

```
kubectl port-forward svc/argocd-server -n argocd 8080:443
```

Then you can connect using:

```
argocd login localhost:8080 --insecure
```

> The username is `admin`
> The password is the name of the Pod. you can get it with `kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2`

### Add Cluster

First make sure that your kubeconfig has both the cluster where Argo-CD is running and the cluster you want to add.  
You see the clusters configured in your kubeconfig with:

```
kubectl config get-clusters
```

Make sure that you're `argocd` CLI is connected to your Argo-CD instance. Once you can talk to your instance using the CLI run

```
argocd cluster add <name-of-the-cluster>
```

### Add Staging Definitions

To add all the definitions for Staging environment run:

```
kubectl apply -f argocd/apps/ -f argocd/
```


[//]: # (EXTERNAL LINKS)

[celduin-infra project]: https://gitlab.com/celduin/infrastructure/celduin-infra
